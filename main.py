from discord import Intents, Message
from discord.ext import interaction, commands

import os
import json
import lavalink

from utils import createLogger

with open("./config.json", "r", encoding="utf8") as f:
    config: dict = json.load(f)


class ART(interaction.AutoShardedClient):
    def __init__(self):
        super().__init__(
            intents=Intents.all(),
            enable_debug_events=True,
            global_sync_command=True,
            command_prefix=commands.when_mentioned_or(config["PREFIX"]),
        )
        self.logger = createLogger()
        self.config = config

    async def on_ready(self):
        self.logger.info("Loading Extensions..")
        for i in os.listdir("./cogs"):
            if i.endswith(".py"):
                try:
                    self.load_extension(f"cogs.{i[:-3]}")
                    self.logger.info(f"Loaded extension {i}")
                except commands.errors.NoEntryPointError:
                    self.logger.error(f"No entry point found for {i}")
        self.load_extension("jishaku")
        self.logger.info("Loaded extension jishaku")
        self.logger.info(f"Logged in as {self.user}")

    async def on_message(self, message: Message):
        if message.author.bot:
            return
        await self.process_commands(message)


if __name__ == "__main__":
    ART().run(config["TOKEN"])
