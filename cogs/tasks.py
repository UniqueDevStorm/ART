from discord import Guild, Message, utils
from discord.ext import commands, tasks

import json
import datetime
from pytz import utc, timezone

from main import ART


class Tasks(commands.Cog):
    def __init__(self, bot: ART):
        self.bot = bot

    @tasks.loop(hours=1)
    async def checkChannel(self):
        guild: Guild = self.bot.get_guild(self.bot.config.get("GUILD"))
        for category in list(filter(lambda x: x.name == "🎨【 작가채널 】", guild.categories)):
            for channel in category.text_channels:
                if channel.topic is None:
                    self.bot.logger.warn(f"Not found topic in {channel.name}")
                    continue
                if guild.get_member(int(channel.topic)) is None:
                    self.bot.logger.warn(f"Not found author in {channel.name}")
                    continue
                try:
                    message: Message = list(
                        filter(
                            lambda x: x.author.id == int(channel.topic),
                            await channel.history(limit=10).flatten(),
                        )
                    )[0]
                    tracked = message.created_at.replace(tzinfo=utc).astimezone(
                        timezone("Asia/Seoul")
                    )
                    if (tracked + datetime.timedelta(days=14)) < datetime.datetime.now(
                        timezone("Asia/Seoul")
                    ):
                        with open("./database.json", "r", encoding="utf8") as f:
                            data = json.load(f)
                        if data["channelLog"].get(str(channel.id)) is None:
                            data["channelLog"][str(channel.id)] = {
                                "timestamp": datetime.datetime.now(
                                    timezone("Asia/Seoul")
                                ).timestamp(),
                            }
                            with open("./database.json", "w", encoding="utf8") as f:
                                json.dump(data, f, ensure_ascii=False, indent=4)
                            return await channel.send(
                                f"<@{channel.topic}> 장기간 미활동으로 24시간 후 채널 삭제합니다! 그림 올리시면 보존되니 참고 바랍니다! "
                                "`이 뒤로는 적어도 14일에 한번씩은 활동 부탁드려요!`"
                            )
                        else:
                            if datetime.datetime.fromtimestamp(
                                data["channelLog"][str(channel.id)]["timestamp"]
                            ) + datetime.timedelta(hours=24) < datetime.datetime.now(
                                timezone("Asia/Seoul")
                            ):
                                await channel.edit(
                                    category=guild.get_channel(
                                        self.bot.config.get("REQUIRE_DELETE_CHANNEL")
                                    ).category,
                                    sync_permissions=True,
                                )
                                data["channelLog"].pop(str(channel.id))
                                with open("./database.json", "w", encoding="utf8") as f:
                                    json.dump(data, f, ensure_ascii=False, indent=4)
                                return await channel.send(
                                    content=f'<@&{self.bot.config.get("VJ")}>'
                                )
                except (IndexError, TypeError):
                    continue


def setup(bot: ART):
    bot.add_cog(Tasks(bot))
