from discord.ext import interaction
from discord.ext import commands, tasks
from discord import Message, utils, Embed, Color, Guild, Member

import json

from main import ART


class Listener(commands.Cog):
    def __init__(self, bot: ART):
        self.bot = bot
        self.changeDatabase.start()

    @tasks.loop(seconds=60)
    async def changeDatabase(self):
        guild: Guild = self.bot.get_guild(self.bot.config.get("GUILD"))
        with open("./database.json", "r", encoding="utf-8") as f:
            data = json.load(f)
        if len(data["privateChannelRequest"]) == 0:
            return
        for i in data["privateChannelRequest"]:
            if guild.get_member(int(i, 16)) is None:
                del data["privateChannelRequest"][i]
                with open("./database.json", "w", encoding="utf-8") as f:
                    json.dump(data, f, ensure_ascii=False, indent=4)
                return

    @commands.Cog.listener()
    async def on_message(self, message: Message):
        if message.channel.id == self.bot.config.get("PHOTO_CHANNEL"):
            if message.attachments:
                await message.add_reaction("❤️")

    @interaction.listener()
    async def on_autocomplete(self, ctx: interaction.AutocompleteContext):
        if (
            utils.get(ctx.guild.roles, id=self.bot.config.get("VJ"))
            not in ctx.guild.get_member(ctx.author.id).roles
        ):
            return await ctx.autocomplete([])
        if ctx.name == "작가처리":
            _id = ctx.options.get("아이디")
            with open("./database.json", "r", encoding="utf-8") as f:
                data = json.load(f)
            return await ctx.autocomplete(
                [
                    interaction.CommandOptionChoice(
                        name=f"{i}({data['privateChannelRequest'][i]}작가)",
                        value=i,
                    )
                    for i in data["privateChannelRequest"]
                    if i.startswith(_id)
                    or data["privateChannelRequest"][i].startswith(_id)
                ]
            )

    @interaction.listener()
    async def on_components(self, ctx: interaction.ComponentsContext):
        if ctx.custom_id == "addRole":
            user = ctx.guild.get_member(ctx.author.id)
            if (
                len(
                    list(
                        filter(
                            lambda x: x.id == self.bot.config.get("VIEWER")
                            or x.id == self.bot.config.get("WRITER"),
                            user.roles,
                        )
                    )
                )
                != 0
            ):
                return await ctx.send(
                    embed=Embed(
                        title="Error!",
                        description="이미 역할을 받으신거 같아요!",
                        color=Color.red(),
                    ),
                    hidden=True,
                )
            await ctx.guild.get_member(ctx.author.id).add_roles(
                utils.get(ctx.guild.roles, id=self.bot.config.get("VIEWER"))
            )
            return await ctx.send(
                embed=Embed(
                    title="완료!",
                    description="앞으로 당신의 활동으로\nART 커뮤니티를 빛내주세요!",
                    color=Color.green(),
                ),
                hidden=True,
            )


def setup(bot: ART):
    bot.add_icog(Listener(bot))
