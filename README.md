<h1 style="text-align: center">ART</h1>
<a href="https://discord.gg/K4CJvbB"><img src="https://img.shields.io/badge/Discord-5865F2?style=flat-square&logo=discord&logoColor=white" alt=""/></a>
<h1>Environment</h1>
<div>
    <li>Windows 11</li>
    <li>Python 3.9.8</li>
</div>
<h1>Installation</h1>
<code lang="bash">
    pip install -r requirements.txt
</code>
<br />
<br />
Fill the root folder with <code>config.json</code> as in <code>config.example.json</code>.
<h1>Usage</h1>
<code lang="bash">
    python main.py
</code>