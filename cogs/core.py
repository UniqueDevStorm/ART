from discord.ext import interaction
from discord import (
    Embed,
    Color,
    NotFound,
    ChannelType,
    Member,
    utils,
    PermissionOverwrite,
    Message,
    errors,
    Forbidden,
    HTTPException,
    Guild,
    TextChannel,
)

import json
import asyncio
import datetime
from typing import List
from pytz import utc, timezone

from main import ART


class Core:
    def __init__(self, bot: ART):
        self.bot = bot

    @interaction.command(name="작가신청", description="개인 작가 채널을 신청합니다.", sync_command=True)
    @interaction.option(name="작가명", description="작가분의 이름을 적어주세요!")
    async def privateChannelRequest(
        self, ctx: interaction.InteractionContext, *, name: str
    ):
        if ctx.channel.id != self.bot.config.get("PRIVATE_CHANNEL_REQUEST"):
            return await ctx.send(
                embed=Embed(
                    title="잘못된 채널입니다.",
                    description=f"<#{self.bot.config.get('PRIVATE_CHANNEL_REQUEST')}> 에 가서 신청해주세요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        for category in list(
            filter(lambda x: x.name == "🎨【 작가채널 】", ctx.guild.categories)
        ):
            for channel in category.channels:
                if channel.topic == str(ctx.author.id):
                    return await ctx.send(
                        embed=Embed(
                            title="이미 신청하셨습니다.",
                            description=f"<#{channel.id}> 에 작가채널이 있습니다!",
                            color=Color.red(),
                        )
                    )
        with open("./database.json", "r", encoding="utf8") as f:
            data = json.load(f)
        if data.get("privateChannelRequest") is None:
            data["privateChannelRequest"] = {}
        if data["privateChannelRequest"].get(hex(ctx.author.id)) is not None:
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description="이미 신청하셨어요!\n관리자분들이 처리중이니 기다려주세요!",
                    color=Color.orange(),
                ),
                hidden=True,
            )
        authorName = name.replace(" ", "").replace("작가", "")
        checkRequest: interaction.InteractionContext = await ctx.send(
            embed=Embed(
                title="개인 작가 채널 신청",
                description=f"신청될 이름 : **{authorName}작가**",
                color=Color.orange(),
            ).set_footer(text="정말로 신청 하시겠습니까?"),
            components=[
                interaction.ActionRow(
                    [
                        interaction.Button(
                            style=3,
                            emoji="✅",
                            custom_id="accept",
                        ),
                        interaction.Button(
                            style=4,
                            emoji="❎",
                            custom_id="decline",
                        ),
                    ]
                )
            ],
        )
        try:
            resp: interaction.ComponentsContext = (
                await self.bot.wait_for_global_component(
                    check=lambda x: x.custom_id in ["accept", "decline"], timeout=60
                )
            )
        except asyncio.TimeoutError:
            return await checkRequest.edit(
                embed=Embed(
                    title="신청 취소",
                    description="60초 시간 초과입니다.\n다시 시도해주세요.",
                    color=Color.red(),
                ),
                components=[],
            )
        else:
            try:
                await resp.defer_update()
            except NotFound:
                pass
            if resp.custom_id == "accept":
                data["privateChannelRequest"][hex(ctx.author.id)] = authorName
                with open("./database.json", "w", encoding="utf8") as f:
                    json.dump(data, f, ensure_ascii=False, indent=4)
                await checkRequest.edit(
                    embed=Embed(
                        title="개인 작가 채널 신청",
                        description="정상적으로 신청되었습니다!",
                        color=Color.green(),
                    ).set_footer(text=f"ID : {hex(ctx.author.id)}"),
                    components=[],
                )
                return await self.bot.get_channel(
                    self.bot.config.get("PRIVATE_CHANNEL_REQUEST_REPLY")
                ).send(
                    content=f"<@&{self.bot.config.get('VJ')}>",
                    embed=Embed(
                        title="개인 작가 채널 신청",
                        description=f"신청자 : {ctx.author.mention}\n채널 이름 : **{authorName}작가**",
                        color=Color.green(),
                    ).set_footer(text=f"ID : {hex(ctx.author.id)}"),
                )
            else:
                return await checkRequest.edit(
                    embed=Embed(
                        title="개인 작가 채널 신청",
                        description="신청이 취소되었습니다.",
                        color=Color.orange(),
                    ),
                    components=[],
                )

    @interaction.command(
        name="작가처리", description="(VJ Only) 작가 처리를 신청합니다.", sync_command=True
    )
    @interaction.option(name="아이디", description="신청 ID를 적어주세요!", autocomplete=True)
    async def privateChannelRequestReply(
        self, ctx: interaction.InteractionContext, _id: str
    ):
        if len(_id.split("(")) != 0:
            _id = _id.split("(")[0]
        if ctx.channel.type == ChannelType.private:
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description=f"잘못된 채널입니다.\n<#{self.bot.config.get('PRIVATE_CHANNEL_REQUEST_REPLY')}> 에서 처리해주세요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        if (
            len(
                list(
                    filter(
                        lambda x: x.id == self.bot.config.get("VJ"), ctx.author.roles
                    )
                )
            )
            == 0
        ):
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description="VJ만 사용할 수 있어요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        if ctx.channel.id != self.bot.config.get("PRIVATE_CHANNEL_REQUEST_REPLY"):
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description=f"잘못된 채널입니다.\n<#{self.bot.config.get('PRIVATE_CHANNEL_REQUEST_REPLY')}> 에서 처리해주세요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        with open("./database.json", "r", encoding="utf8") as f:
            data = json.load(f)
        if data["privateChannelRequest"].get(_id) is None:
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description=f"잘못된 아이디입니다.\n신청한 아이디를 정확히 입력해주세요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        checkAccept = await ctx.send(
            embed=Embed(
                title="작가 신청 처리",
                description=f"작가 신청을 승인하시겠습니까?\nID : `{_id}`\n채널 이름 : `{data['privateChannelRequest'][_id]}작가`",
                color=Color.orange(),
            ),
            components=[
                interaction.ActionRow(
                    [
                        interaction.Button(
                            style=3,
                            emoji="✅",
                            custom_id="accept",
                        ),
                        interaction.Button(
                            style=4,
                            emoji="❎",
                            custom_id="decline",
                        ),
                    ]
                )
            ],
        )
        try:
            resp: interaction.ComponentsContext = (
                await self.bot.wait_for_global_component(
                    check=lambda x: x.custom_id in ["accept", "decline"],
                    timeout=60,
                )
            )
        except asyncio.TimeoutError:
            return await checkAccept.edit(
                embed=Embed(
                    title="작가 신청 처리",
                    description="60초 시간 초과입니다.\n다시 시도해주세요.",
                    color=Color.red(),
                ),
                components=[],
            )
        else:
            try:
                await resp.defer_update()
            except NotFound:
                pass
            userId = int(_id, 16)
            user: Member = ctx.guild.get_member(userId)
            if user is None:
                return await ctx.send(
                    embed=Embed(
                        title="Error!",
                        description=f"해당 유저를 찾지 못했습니다.",
                        color=Color.red(),
                    )
                )
            if resp.custom_id == "accept":
                await user.remove_roles(
                    utils.get(ctx.guild.roles, id=self.bot.config.get("VIEWER"))
                )
                await user.add_roles(
                    utils.get(ctx.guild.roles, id=self.bot.config.get("WRITER"))
                )
                try:
                    channel = await ctx.guild.create_text_channel(
                        f'{data["privateChannelRequest"][_id]}작가',
                        category=sorted(
                            list(
                                filter(
                                    lambda x: x.name == "🎨【 작가채널 】",
                                    ctx.guild.categories,
                                )
                            ),
                            key=lambda x: len(x.channels),
                        )[0],
                        overwrites={
                            ctx.guild.default_role: PermissionOverwrite(
                                view_channel=False
                            ),
                            ctx.guild.get_role(
                                self.bot.config.get("VIEWER")
                            ): PermissionOverwrite(
                                view_channel=True, send_messages=False
                            ),
                            ctx.guild.get_role(
                                self.bot.config.get("WRITER")
                            ): PermissionOverwrite(
                                view_channel=True, send_messages=False
                            ),
                            ctx.guild.get_member(userId): PermissionOverwrite(
                                view_channel=True,
                                send_messages=True,
                                manage_messages=True,
                            ),
                        },
                        topic=userId,
                    )
                except errors.HTTPException:
                    createCategory = await ctx.guild.create_category(
                        "🎨【 작가채널 】",
                        position=ctx.guild.categories[
                            ctx.guild.categories.index(
                                list(
                                    filter(
                                        lambda x: x.name == "🎨【 작가채널 】",
                                        ctx.guild.categories,
                                    )
                                )[-1]
                            )
                            + 1
                        ].position
                        - 1,
                    )
                    channel = await ctx.guild.create_text_channel(
                        f'{data["privateChannelRequest"][_id]}작가',
                        category=createCategory,
                        overwrites={
                            ctx.guild.default_role: PermissionOverwrite(
                                view_channel=False
                            ),
                            ctx.guild.get_role(
                                self.bot.config.get("VIEWER")
                            ): PermissionOverwrite(
                                view_channel=True, send_messages=False
                            ),
                            ctx.guild.get_role(
                                self.bot.config.get("WRITER")
                            ): PermissionOverwrite(
                                view_channel=True, send_messages=False
                            ),
                            ctx.guild.get_member(userId): PermissionOverwrite(
                                view_channel=True, send_messages=True
                            ),
                        },
                        topic=userId,
                    )
                try:
                    await user.send(
                        embed=Embed(
                            title="작가 신청 처리",
                            description=f"작가 신청이 승인되었습니다!\n당신의 작품으로 ART 커뮤니티를 빛내주세요!\n<#{channel.id}>",
                            color=Color.green(),
                        )
                    )
                except (HTTPException, Forbidden):
                    await self.bot.get_channel(
                        self.bot.config.get("PRIVATE_CHANNEL_REQUEST")
                    ).send(
                        content=user.mention,
                        embed=Embed(
                            title="작가 신청 처리",
                            description=f"작가 신청이 승인되었습니다!\n당신의 작품으로 ART 커뮤니티를 빛내주세요!\n<#{channel.id}>",
                            color=Color.green(),
                        ),
                    )
                await checkAccept.edit(
                    embed=Embed(
                        title="작가 신청 처리",
                        description="정상적으로 처리되었습니다!",
                        color=Color.green(),
                    ),
                    components=[],
                )
            if resp.custom_id == "decline":
                await checkAccept.edit(
                    embed=Embed(
                        title="작가 신청 처리",
                        description="신청을 거부 처리 하겠습니다.\n사유는 무엇인가요?",
                        color=Color.red(),
                    ),
                    components=[],
                )
                try:
                    resp: Message = await self.bot.wait_for(
                        "message",
                        check=lambda m: m.author == ctx.author
                        and m.channel == ctx.channel,
                        timeout=60,
                    )
                except asyncio.TimeoutError:
                    return await checkAccept.edit(
                        embed=Embed(
                            title="신청 취소",
                            description="60초 시간 초과입니다.\n다시 시도해주세요.",
                            color=Color.red(),
                        ),
                    )
                else:
                    await user.send(
                        embed=Embed(
                            title="작가 신청 처리",
                            description=f'관리자에 의해 신청이 반려되었습니다.\n사유 : **{"None" if resp.content == "" else resp.content}**',
                            color=Color.red(),
                        ).set_footer(
                            text=f"처리자 : {ctx.author}", icon_url=ctx.author.avatar_url
                        )
                    )
                    await checkAccept.edit(
                        embed=Embed(
                            title="작가 신청 처리",
                            description=f'정상적으로 처리되었습니다!\n사유 : **{"None" if resp.content == "" else resp.content}**',
                            color=Color.red(),
                        ).set_footer(
                            text=f"처리자 : {ctx.author}", icon_url=ctx.author.avatar_url
                        )
                    )
            data["privateChannelRequest"].pop(_id)
            with open("./database.json", "w", encoding="utf8") as f:
                json.dump(data, f, ensure_ascii=False, indent=4)

    @interaction.command(
        name="이모지", description="(VJ Only)규칙을 전송하고, 이모지로 역할을 받을 수 있습니다."
    )
    @interaction.guild_only()
    async def emojiCheck(self, ctx: interaction.InteractionContext):
        if (
            len(
                list(
                    filter(
                        lambda x: x.id == self.bot.config.get("VJ"), ctx.author.roles
                    )
                )
            )
            == 0
        ):
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description="VJ만 사용할 수 있어요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        rule = """
📌 커미션 / 리퀘스트 규칙
```
🎨 커미션 작품을 상업적 용도로 쓰는것을 허용하지 않습니다.
- 커미션 작품 타 사이트에 업로드 시 출처를 남겨주세요.

🎨 미리 공지한 작업기간을 가능한 지켜주시고 불가능할시엔 고객님께 미리 말씀드리고 책임져주세요.
- 그림러의 그림이 진짜 본인그림이 맞는지 제대로 확인해주세요.

🎨 돈 먹튀로 인한 책임은 서버에서 지지 않습니다.
- 단 그사람을 추방하는등, 서버내에서 해줄수있는 조취는 해드립니다.

🎨 단순 변심으로 인한 커미션신청의 취소는 불가능합니다.
- 개인용으로 쓸 프로필, 배경 등은 가능하지만 출처를 밝히지 않는 무단 업로드를 금지합니다.

🎨 커미션 작품을 2차 가공, 수정하지 않습니다.
- 커미션 작품을 도용하면 저희 서버에서 밴하겠습니다.
```

📌 그림 규칙
```
🎨 작가들은 14일마다 최소 1개의 작품이라도 내야합니다.
- 만약 못지킬시 갠디로 통보 후 삭제합니다.

🎨 트레이싱을 이용했다면 트레이용 했다고 꼭 말해주세요.
- 원본과 같이 올려도 되며, 출처 표기해도 됩니다.

🎨 정치적이거나, 선정적이거나, 술 & 담배, 욕설이 포함된 그림은 꼭 스포일러를 덧붙여 주세요.
- 스포일러가 없다면 갠메로 말하겠습니다.
```

📌 서버 규칙
```
🎨 채팅창에 욕설과 패드립, 섹드립 할시 'Level 1'
🎨 커미션 / 리퀘스트 규칙 & 그림 규칙을 어길시 '운영진 상의후 결정'
🎨 연합서버가 아닌 타서버를 갠메로 홍보시 '추방/차단'
```

📌 경고 제도
```
🎨 Level 1 : 경고 1회
🎨 Level 2 : 경고 2회
🎨 Level 3 : 경고 3회
🎨 추방/차단 : 서버 추방 및 차단
```
        """
        await interaction.MessageSendable(
            state=self.bot.connection, channel=self.bot.get_channel(ctx.channel.id)
        ).send(
            content=rule,
            components=[
                interaction.ActionRow(
                    [
                        interaction.Button(
                            style=3, emoji="🎨", label="역할 받기", custom_id="addRole"
                        )
                    ]
                )
            ],
        )
        return await ctx.send(
            embed=Embed(title="정상적으로 전송!", color=Color.green()), hidden=True
        )

    @interaction.command(name="tracker", description="(VJ Only) 모든 작가 채널들을 추적합니다.")
    async def tracking(self, ctx: interaction.InteractionContext):
        if (
            len(
                list(
                    filter(
                        lambda x: x.id == self.bot.config.get("VJ"), ctx.author.roles
                    )
                )
            )
            == 0
        ):
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description="VJ만 사용할 수 있어요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        await ctx.defer()
        data = ""
        outMembers = ""
        guild: Guild = self.bot.get_guild(self.bot.config.get("GUILD"))
        for category in list(
            filter(
                lambda x: x.name == "🎨【 작가채널 】",
                guild.categories,
            )
        ):
            for channel in category.channels:
                if guild.get_member(int(channel.topic)) is None:
                    outMembers += f"{channel.mention}\n"
                    continue
                try:
                    message = list(
                        filter(
                            lambda x: x.author.id == int(channel.topic),
                            await channel.history(limit=10).flatten(),
                        )
                    )[0]
                except (IndexError, TypeError):
                    continue
                if (
                    message.created_at.replace(tzinfo=utc).astimezone(
                        timezone("Asia/Seoul")
                    )
                    + datetime.timedelta(days=14)
                ) < datetime.datetime.now(timezone("Asia/Seoul")):
                    data += f"{channel.mention} : {datetime.datetime.now(timezone('Asia/Seoul')) - (message.created_at.replace(tzinfo=utc).astimezone(timezone('Asia/Seoul')) + datetime.timedelta(days=14))}\n"
        await ctx.send(
            embed=Embed(
                title="Tracking",
                description=f"**나간 사람** : \n{outMembers}\n**조치 필요 채널** : \n{data}",
                color=Color.green(),
            )
        )

    @interaction.command(name="warn", description="(VJ Only) 해당 채널들에게 경고를 보냅니다.")
    @interaction.option(name="channels", description="경고를 보낼 채널들을 공백으로 구분하여 적어주세요!")
    async def warn(self, ctx: interaction.InteractionContext, channels: str):
        if (
            len(
                list(
                    filter(
                        lambda x: x.id == self.bot.config.get("VJ"), ctx.author.roles
                    )
                )
            )
            == 0
        ):
            return await ctx.send(
                embed=Embed(
                    title="Error!",
                    description="VJ만 사용할 수 있어요!",
                    color=Color.red(),
                ),
                hidden=True,
            )
        await ctx.defer()
        channelList: List[TextChannel] = list(
            map(lambda x: self.bot.get_channel(int(x)), channels.split(" "))
        )
        for channel in channelList:
            if channel.topic is None:
                continue
            await channel.send(
                content=f"<@{channel.topic}> 장기간 미활동으로 24시간 후 채널 삭제합니다! 그림 올리시면 보존되니 참고 바랍니다! "
                        "`이 뒤로는 적어도 14일에 한번씩은 활동 부탁드려요!`"
            )
        return await ctx.send('Success!')


def setup(bot: ART):
    bot.add_icog(Core(bot))
